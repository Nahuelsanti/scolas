import { Component } from '@angular/core'

@Component({
  selector: 'app-recurring-payment',
  templateUrl: './recurring-payment.component.html',
  styleUrls: ['./recurring-payment.component.css'],
})
export class RecurringPaymentComponent {}
